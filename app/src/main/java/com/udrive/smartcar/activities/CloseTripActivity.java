package com.udrive.smartcar.activities;

import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.udrive.smartcar.R;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class CloseTripActivity extends AppCompatActivity {

    private Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_trip);
        geocoder = new Geocoder(this, Locale.getDefault());
        populateViewObjects(getIntent().getExtras());
    }

    private void populateViewObjects(Bundle extras) {
        try {
            paintAddress(R.id.from_address, extras.getDouble("startLatitude"), extras.getDouble("startLongitude"));
            paintAddress(R.id.to_address, extras.getDouble("endLatitude"), extras.getDouble("endLongitude"));
            paintAmount(extras.getDouble("amount"));
            paintDuration(extras.getString("startTime"), extras.getString("endTime"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void paintDuration(String startTime, String endTime) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("IST"));
        Date startDate = format.parse(startTime);
        Date endDate = format.parse(endTime);
        long timeDifference = endDate.getTime() - startDate.getTime();
        TextView durationView = (TextView) findViewById(R.id.duration);
        durationView.setText(String.valueOf(timeDifference / 1000 / 60 / 60) + " hrs");
    }

    private void paintAmount(double amount) {
        TextView amountView = (TextView) findViewById(R.id.amount);
        amountView.setText(amountView.getText() + String.valueOf(amount));
    }

    private void paintAddress(int viewId, double latitude, double longitude) throws IOException {
        Address address = geocoder.getFromLocation(latitude, longitude, 1).get(0);
        TextView addressView = (TextView) findViewById(viewId);
        addressView.setText(address.getAddressLine(0));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_close_trip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void submit(View view) {
    }
}
