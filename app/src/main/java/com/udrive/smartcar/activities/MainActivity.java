package com.udrive.smartcar.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.udrive.smartcar.R;
import com.udrive.smartcar.api.CarBookingService;
import com.udrive.smartcar.api.CarService;
import com.udrive.smartcar.api.GooglePlacesService;
import com.udrive.smartcar.api.responses.CarBooking;
import com.udrive.smartcar.api.responses.GoogleDirectionResponse;
import com.udrive.smartcar.api.responses.GooglePlace;
import com.udrive.smartcar.api.responses.GooglePlaceLocation;
import com.udrive.smartcar.api.responses.GooglePlacesResponse;
import com.udrive.smartcar.api.responses.UDriveLocation;
import com.udrive.smartcar.clients.GooglePlacesClient;
import com.udrive.smartcar.clients.RetrofitClient;
import com.udrive.smartcar.clients.UDriveGoogleAPIClient;
import com.udrive.smartcar.services.LocationService;
import com.udrive.smartcar.util.BitmapUtil;
import com.udrive.smartcar.util.PolyDecoder;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.udrive.smartcar.util.Constants.GOOGLE_API_KEY;
import static com.udrive.smartcar.util.Constants.LOCATION_PERMISSION_REQUEST_CODE;
import static com.udrive.smartcar.util.Constants.MAP_ZOOM_LEVEL;
import static com.udrive.smartcar.util.Constants.NEARBY_PLACES_RADIUS;
import static com.udrive.smartcar.util.Constants.NEARBY_PLACES_TYPES;
import static com.udrive.smartcar.util.Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE;

public class MainActivity extends AppCompatActivity
        implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener, OnMapReadyCallback {

    private static final String TAG = MainActivity.class.getSimpleName();

    private GoogleApiClient googleApiClient;
    public static String DEVICE_ID;
    public static LocationListener locationListener;
    private GoogleMap map;
    private Marker cabMarker;
    private UDriveLocation source;
    private UDriveLocation destination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DEVICE_ID = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        locationListener = this;
        googleApiClient = UDriveGoogleAPIClient.init(this);
        initMap();
    }

    @Override
    protected void onStart() {
        if(!googleApiClient.isConnected()) {
            googleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    protected void onStop() {
//        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates();
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (!isLocationPermissionGranted()) return;
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        source = new UDriveLocation(location.getLatitude(), location.getLongitude());
        updatePositionInMap(location);
        updatePositionInServer(location);
    }

    private void updatePositionInMap(final Location location) {
        LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
        cabMarker.setPosition(position);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, MAP_ZOOM_LEVEL), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                fetchNearbyPlaces(location);
            }

            @Override
            public void onCancel() {
            }
        });
    }

    private void fetchNearbyPlaces(Location location) {
        String formattedLocation = location.getLatitude() + "," + location.getLongitude();
        GooglePlacesService googlePlacesService = GooglePlacesClient.getInstance().create(GooglePlacesService.class);
        Call<GooglePlacesResponse> call = googlePlacesService.fetchNearbyPlaces(formattedLocation, NEARBY_PLACES_RADIUS, NEARBY_PLACES_TYPES, GOOGLE_API_KEY);
        call.enqueue(new Callback<GooglePlacesResponse>() {
            @Override
            public void onResponse(Call<GooglePlacesResponse> call, Response<GooglePlacesResponse> response) {
                for(GooglePlace googlePlace : response.body().getResults()) {
                    new DownloadMapIconAndPlotGooglePlaceTask().execute(googlePlace);
                }
            }
            @Override
            public void onFailure(Call<GooglePlacesResponse> call, Throwable t) {
            }
        });
    }

    private void updatePositionInServer(Location location) {
        UDriveLocation UDriveLocation = new UDriveLocation(location.getLatitude(), location.getLongitude());
        CarService carService = RetrofitClient.getInstance().create(CarService.class);
        Call<UDriveLocation> call = carService.updateLocation(DEVICE_ID, UDriveLocation);
        call.enqueue(new Callback<UDriveLocation>() {
            @Override
            public void onResponse(Call<UDriveLocation> call, Response<UDriveLocation> response) {
            }

            @Override
            public void onFailure(Call<UDriveLocation> call, Throwable t) {
            }
        });
    }

    private void initMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.car);
        cabMarker = map.addMarker(new MarkerOptions()
                .position(new LatLng(0, 0))
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapUtil.minimize(bitmap))));
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean isLocationPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    private void startLocationUpdates() {
        startService(new Intent(LocationService.INTENT_ACTION, null, this, LocationService.class));
    }

    public void closeTrip(View view) {
        CarBookingService carBookingService = RetrofitClient.getInstance().create(CarBookingService.class);
        Call<CarBooking> call = carBookingService.closeTrip(DEVICE_ID);
        call.enqueue(new Callback<CarBooking>() {
            @Override
            public void onResponse(Call<CarBooking> call, Response<CarBooking> response) {
                CarBooking carBooking = response.body();
                Intent intent = new Intent(getApplicationContext(), CloseTripActivity.class);
                intent.putExtra("startTime", carBooking.getStartTime());
                intent.putExtra("endTime", carBooking.getEndTime());
                intent.putExtra("startLatitude", carBooking.getStartLatitude());
                intent.putExtra("startLongitude", carBooking.getStartLongitude());
                intent.putExtra("endLatitude", carBooking.getEndLatitude());
                intent.putExtra("endLongitude", carBooking.getEndLongitude());
                intent.putExtra("distance", carBooking.getDistance());
                intent.putExtra("amount", carBooking.getAmount());
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<CarBooking> call, Throwable t) {

            }
        });
    }

    public void launchAutoComplete(View view) {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e(TAG, "Auto complete widget cannot be launched");
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, "Auto complete widget cannot be launched");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                destination = new UDriveLocation(place.getLatLng().latitude, place.getLatLng().longitude);
                paintDestination(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                Log.e(TAG, "User cancelled");
            }
        }
    }

    private void paintDestination(Place place) {
        Button destinationPicker = (Button) findViewById(R.id.destinationPicker);
        destinationPicker.setText(place.getAddress());
        destinationPicker.setTextColor(Color.BLACK);
        drawRoute();
    }

    private void drawRoute() {
        GooglePlacesService googlePlacesService = GooglePlacesClient.getInstance().create(GooglePlacesService.class);
        Call<GoogleDirectionResponse> call = googlePlacesService.getDirection(source.formattedString(), destination.formattedString(), GOOGLE_API_KEY);
        call.enqueue(new Callback<GoogleDirectionResponse>() {
            @Override
            public void onResponse(Call<GoogleDirectionResponse> call, Response<GoogleDirectionResponse> response) {
                GoogleDirectionResponse googleDirectionResponse = response.body();
                String points = googleDirectionResponse.getRoutes().get(0).getOverViewPolyLine().getPoints();
                List<LatLng> latLngs = PolyDecoder.decodePoly(points);
                Polyline line = map.addPolyline(new PolylineOptions()
                        .addAll(latLngs)
                        .width(12)
                        .color(Color.parseColor("#05b1fb"))
                        .geodesic(true)
                );
            }

            @Override
            public void onFailure(Call<GoogleDirectionResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });

    }

    private class DownloadMapIconAndPlotGooglePlaceTask extends AsyncTask<GooglePlace, Void, GooglePlace> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @Override
        protected GooglePlace doInBackground(GooglePlace... googlePlaces) {
            GooglePlace googlePlace = googlePlaces[0];
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(googlePlace.getIcon());
                urlConnection = (HttpURLConnection) url.openConnection();
                googlePlace.setBitmap(BitmapFactory.decodeStream(urlConnection.getInputStream()));
                return googlePlace;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if(urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(GooglePlace googlePlace) {
            super.onPostExecute(googlePlace);
            GooglePlaceLocation googlePlaceLocation = googlePlace.getGeometry().getLocation();
            MarkerOptions markerOptions = new MarkerOptions()
                    .title(googlePlace.getName())
                    .position(new LatLng(googlePlaceLocation.getLatitude(), googlePlaceLocation.getLongitude()))
                    .icon(BitmapDescriptorFactory.fromBitmap(BitmapUtil.minimize(googlePlace.getBitmap())));
            map.addMarker(markerOptions);
        }
    }
}
