package com.udrive.smartcar.util;

public class Constants {

    public static int LOCATION_PERMISSION_REQUEST_CODE = 801;
    public static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 802;

    public static int LOCATION_UPDATE_INTERVAL = 10000;
    public static int LOCATION_FASTEST_INTERVAL = 5000;
    public static int LOCATION_DISPLACEMENT = 10;

    public static String GOOGLE_API_KEY = "AIzaSyAUxknACzJ5czuXh7rxlCc26b1iny_tOdU";

    public static int MAP_ZOOM_LEVEL = 17;
    public static int NEARBY_PLACES_RADIUS = 1000;
    public static String NEARBY_PLACES_TYPES = "atm|bakery|bank|car_repair|church|hindu_temple|mosque|shopping_mall|taxi_stand|grocery_or_supermarket|park|police|parking|hospital|restaurant|cafe";

    public static String UDRIVE_SERVICE_URL = "https://udrive-service.herokuapp.com/";
    public static String GOOGLE_PLACES_URL = "https://maps.googleapis.com";
}
