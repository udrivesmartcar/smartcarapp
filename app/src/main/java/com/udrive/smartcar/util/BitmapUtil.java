package com.udrive.smartcar.util;

import android.graphics.Bitmap;

public class BitmapUtil {

    public static Bitmap minimize(Bitmap bitmap) {
        return Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 2, bitmap.getHeight() / 2, false);
    }
}
