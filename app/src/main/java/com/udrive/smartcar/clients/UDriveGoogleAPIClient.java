package com.udrive.smartcar.clients;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.udrive.smartcar.activities.MainActivity;

public class UDriveGoogleAPIClient {

    private static GoogleApiClient googleApiClient;

    public static GoogleApiClient init(MainActivity context) {
        if(googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(context)
                    .addOnConnectionFailedListener(context)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
        }
        return googleApiClient;
    }

    public static GoogleApiClient get() {
        return googleApiClient;
    }
}
