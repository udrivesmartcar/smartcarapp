package com.udrive.smartcar.clients;

import com.udrive.smartcar.util.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static com.udrive.smartcar.util.Constants.GOOGLE_PLACES_URL;

public class GooglePlacesClient {

    private static Retrofit googlePlacesClient;

    public static Retrofit getInstance() {
        if(googlePlacesClient == null) {
            googlePlacesClient = new Retrofit.Builder()
                    .baseUrl(GOOGLE_PLACES_URL)
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();
        }
        return googlePlacesClient;
    }
}
