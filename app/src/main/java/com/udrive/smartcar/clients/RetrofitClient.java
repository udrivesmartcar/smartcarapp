package com.udrive.smartcar.clients;

import com.udrive.smartcar.util.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static com.udrive.smartcar.util.Constants.UDRIVE_SERVICE_URL;

public class RetrofitClient {

    private static Retrofit retrofit;

    public static Retrofit getInstance() {
        if(retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(UDRIVE_SERVICE_URL)
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
