package com.udrive.smartcar.api.responses;

import android.graphics.Bitmap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Getter
@Setter
public class GooglePlace {
    private String id;
    private String icon;
    private String name;
    private String vicinity;
    private String place_id;
    private Geometry geometry;
    private Bitmap bitmap;
}
