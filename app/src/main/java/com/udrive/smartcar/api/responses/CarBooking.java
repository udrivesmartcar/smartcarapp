package com.udrive.smartcar.api.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Getter
@Setter
public class CarBooking {

    @JsonProperty("start_time")
    private String startTime;

    @JsonProperty("end_time")
    private String endTime;

    @JsonProperty("start_latitude")
    private double startLatitude;

    @JsonProperty("start_longitude")
    private double startLongitude;

    @JsonProperty("end_latitude")
    private double endLatitude;

    @JsonProperty("end_longitude")
    private double endLongitude;

    @JsonProperty("distance")
    private double distance;

    @JsonProperty("amount")
    private double amount;

    public UDriveLocation getStartLocation() {
        return new UDriveLocation(startLatitude, startLongitude);
    }

    public UDriveLocation getEndLocation() {
        return new UDriveLocation(endLatitude, endLongitude);
    }
}
