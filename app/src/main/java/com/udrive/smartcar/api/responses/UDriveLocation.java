package com.udrive.smartcar.api.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Getter
@Setter
public class UDriveLocation {
    private double latitude;
    private double longitude;

    public String formattedString() {
        return latitude + "," + longitude;
    }
}
