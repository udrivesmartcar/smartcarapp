package com.udrive.smartcar.api;

import com.udrive.smartcar.api.responses.UDriveLocation;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CarService {

    @POST("cars/{device_id}/update_location")
    Call<UDriveLocation> updateLocation(@Path("device_id") String deviceId, @Body UDriveLocation UDriveLocation);
}
