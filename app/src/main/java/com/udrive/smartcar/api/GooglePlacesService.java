package com.udrive.smartcar.api;

import com.udrive.smartcar.api.responses.GoogleDirectionResponse;
import com.udrive.smartcar.api.responses.GooglePlacesResponse;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GooglePlacesService {

    @GET("maps/api/place/nearbysearch/json?sensor=true")
    Call<GooglePlacesResponse> fetchNearbyPlaces(@Query("location") String location,
                                              @Query("radius") long radius, @Query("types") String types,
                                              @Query("key") String key);

    @POST("maps/api/directions/json?sensor=false&mode=driving&alternatives=true")
    Call<GoogleDirectionResponse> getDirection(@Query("origin") String origin, @Query("destination") String destination, @Query("key") String key);
}
