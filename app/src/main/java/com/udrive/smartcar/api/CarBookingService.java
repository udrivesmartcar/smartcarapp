package com.udrive.smartcar.api;

import com.udrive.smartcar.api.responses.CarBooking;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CarBookingService {

    @POST("car_bookings/{device_id}/close_trip")
    Call<CarBooking> closeTrip(@Path("device_id") String deviceId);
}
