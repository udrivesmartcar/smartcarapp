package com.udrive.smartcar.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.udrive.smartcar.clients.UDriveGoogleAPIClient;

import static com.udrive.smartcar.activities.MainActivity.locationListener;
import static com.udrive.smartcar.util.Constants.LOCATION_DISPLACEMENT;
import static com.udrive.smartcar.util.Constants.LOCATION_FASTEST_INTERVAL;
import static com.udrive.smartcar.util.Constants.LOCATION_UPDATE_INTERVAL;

public class LocationService extends Service {

    public static final String INTENT_ACTION = "com.udrive.smartcar.services.LOCATION_SERVICE";

    private static LocationRequest locationRequest;

    @Override
    public void onCreate() {
        if(locationRequest == null) {
            locationRequest = new LocationRequest();
            locationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
            locationRequest.setFastestInterval(LOCATION_FASTEST_INTERVAL);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//            locationRequest.setSmallestDisplacement(LOCATION_DISPLACEMENT);
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        GoogleApiClient googleApiClient = UDriveGoogleAPIClient.get();
        if(googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
